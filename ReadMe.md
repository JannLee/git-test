# 제목

## 테스트

- 신입사원 교육

### Git

- Git에 대한 교육 시작
- 발표 자료 작성

### 상태

- 목이 많이 마르네요.
- 목이 진짜 말라요.

 ```plantuml
 !theme plain
  box "긴급 임시 브랜치" #Orange
    collections hotfixes as h
  end box
  box "영구 브랜치" #LightBlue
    database master as m
    database develop as d
  end box
  box "정기 임시 브랜치" #LightGray
    collections features as f
    participant release as r
    collections issues as i
    collections license as l
  end box
  activate m
  note over m : 1.0.0
  m -> d : 차기 버전 시작
  activate d
  d -> f : 기능 개발 시작
  rnote over f : feature/1.0.0_1234_add_awesome
  activate f
  h <- m : 긴급 이슈
  activate h
  rnote over h : hotfix/1.0.0_2345_app_crash
  h -/o m : 긴급 패치
  destroy h
  note over m : 1.0.1
  m ->o d : 수정 사항 반영
  m -->>o f : 수정 사항 반영
  d o\- f : 기능 개발 완료
  destroy f
  d -> r : 정기 출시 시작
  activate r
  rnote over r : release/1.1.0
  r -> i : 이슈 발생
  activate i
  rnote over i : issue/1.1.0_3456_unprotect_app
  h <- m : 긴급 이슈
  activate h
  rnote over h : hotfix/1.0.1_4567_bsod
  h -/o m : 긴급 패치
  destroy h
  note over m : 1.0.2
  m ->o d : 수정 사항 반영
  m -->o r : 수정 사항 반영
  r -->>o i : 수정 사항 반영
  r o\- i : 이슈 해결
  destroy i
  d o<- r : 수정 사항 반영
  m o<- r : 차기 버전 출시
  destroy r
  note over m : 1.1.0
  ```
  
